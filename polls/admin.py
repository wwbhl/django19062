from django.contrib import admin

from polls.models import Subject, Teacher, User


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('no', 'name', 'intro', 'is_hot')
    list_display_links = ('no', 'name')
    search_fields = ('name', )
    ordering = ('no', )


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('no', 'name', 'sex', 'birth', 'good_count', 'bad_count', 'subject')
    list_display_links = ('no', 'name')
    ordering = ('no', )


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'tel', 'reg_date', 'last_visit')
    ordering = ('no', )


admin.site.register(Subject, SubjectAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(User, UserAdmin)
