from bpmappers import RawField, DelegateField
from bpmappers.djangomodel import ModelMapper

from polls.models import Subject, Teacher


class SubjectMapper(ModelMapper):
    """学科映射器"""
    isHot = RawField('is_hot')

    class Meta:
        model = Subject
        exclude = ('is_hot', )


class SubjectSimpleMapper(ModelMapper):
    """学科简单映射器"""

    class Meta:
        model = Subject
        fields = ('no', 'name')


class TeacherMapper(ModelMapper):
    """老师映射器"""
    goodCount = RawField('good_count')
    badCount = RawField('bad_count')

    class Meta:
        model = Teacher
        exclude = ('good_count', 'bad_count', 'subject')
